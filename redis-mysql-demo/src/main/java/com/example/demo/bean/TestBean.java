package com.example.demo.bean;

import java.io.Serializable;

public class TestBean implements Serializable {

	/**
	 * chen hao
	 */
	private static final long serialVersionUID = 5681475083950029761L;
	private String id;
	private String name;
	private String password;

	public TestBean() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "TestBean [id=" + id + ", name=" + name + ", password=" + password + "]";
	}

}
