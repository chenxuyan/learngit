package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.bean.TestBean;
import com.example.demo.service.api.TestBeanService;

@RestController
public class TestBeanController {
	@Autowired
	private TestBeanService testBeanService;

	@RequestMapping(value = "findTestBeanById", method = { RequestMethod.GET })
	public TestBean findTestBeanById(@RequestParam(value = "id", required = true) String id) {
		TestBean testBean = testBeanService.findTestBeanById(id);
		return testBean;
	}

	@RequestMapping(value = "addTestBean", method = { RequestMethod.POST })
	public int addTestBean(@RequestParam(value = "testBean", required = true) TestBean testBean) {
		int i = testBeanService.addTestBean(testBean);
		return i;
	}

	@RequestMapping(value = "delTestBean", method = { RequestMethod.GET })
	public int delTestBean(@RequestParam(value = "id", required = true) String id) {
		int i = testBeanService.delTestBean(id);
		return i;
	}

}
