package com.example.demo.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

import com.example.demo.bean.TestBean;

@Mapper
@CacheConfig(cacheNames = "test")
public interface TestBeanMapper {
	@Cacheable(key = "#p0")
	TestBean findTestBeanById(String id);
	/** 
     * 新增或修改时 
     */  
    @CachePut(key = "#p0.id")  
	int addTestBean(TestBean testBean);
    
	int delTestBean(String id);
}
