package com.example.demo.service.imp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.bean.TestBean;
import com.example.demo.mapper.TestBeanMapper;
import com.example.demo.service.api.TestBeanService;
@Service
public class TestBeanServiceImp  implements TestBeanService{
  
	@Autowired
	TestBeanMapper testBeanMapper;
	
	
	@Override
	public TestBean findTestBeanById(String id) {
		// TODO Auto-generated method stub
		return testBeanMapper.findTestBeanById(id);
	}

	@Override
	public int addTestBean(TestBean testBean) {
		// TODO Auto-generated method stub
		return testBeanMapper.addTestBean(testBean);
	}

	@Override
	public int delTestBean(String id) {
		// TODO Auto-generated method stub
		return testBeanMapper.delTestBean(id);
	}

}
