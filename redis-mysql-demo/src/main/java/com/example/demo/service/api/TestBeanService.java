package com.example.demo.service.api;



import com.example.demo.bean.TestBean;

public interface TestBeanService {
	TestBean findTestBeanById(String id);

	int addTestBean(TestBean testBean);

	int delTestBean(String id);
}
